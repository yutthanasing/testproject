USE [MVC]
GO
/****** Object:  Table [dbo].[tb_Menu]    Script Date: 2/11/2561 14:44:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Menu](
	[MenuID] [int] IDENTITY(1,1) NOT NULL,
	[MenuParentID] [int] NULL,
	[MenuOrder] [int] NOT NULL,
	[MenuName] [nvarchar](250) NULL,
	[Controller] [nvarchar](150) NULL,
	[Action] [nvarchar](150) NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](35) NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [varchar](35) NULL,
 CONSTRAINT [PK_tb_Menu] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_Permission]    Script Date: 2/11/2561 14:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Permission](
	[PermissionID] [int] IDENTITY(1,1) NOT NULL,
	[Permission] [nvarchar](50) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](35) NOT NULL,
 CONSTRAINT [PK_tb_Permission] PRIMARY KEY CLUSTERED 
(
	[PermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_Profile]    Script Date: 2/11/2561 14:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Profile](
	[ProfileID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[TitleName] [varchar](50) NOT NULL,
	[FirstName] [varchar](150) NOT NULL,
	[LastName] [varchar](150) NOT NULL,
	[Address1] [varchar](250) NULL,
	[Address2] [varchar](150) NULL,
	[Province] [varchar](10) NULL,
	[District] [varchar](10) NULL,
	[SubDistrict] [varchar](10) NULL,
	[Mobile] [varchar](20) NULL,
	[Phone] [varchar](20) NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](35) NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [varchar](35) NULL,
 CONSTRAINT [PK_tb_Profile] PRIMARY KEY CLUSTERED 
(
	[ProfileID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_Role]    Script Date: 2/11/2561 14:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_Role](
	[RoleID] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](35) NOT NULL,
	[UpdateDate] [datetime] NULL,
	[UpdateBy] [varchar](35) NULL,
 CONSTRAINT [PK_tb_Role] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_RoleMenu]    Script Date: 2/11/2561 14:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_RoleMenu](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[RoleID] [int] NOT NULL,
	[MenuID] [int] NOT NULL,
	[PermissionID] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
	[CreateBy] [varchar](35) NOT NULL,
 CONSTRAINT [PK_tb_RoleMenu] PRIMARY KEY CLUSTERED 
(
	[MenuID] ASC,
	[PermissionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_User]    Script Date: 2/11/2561 14:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[Email] [nvarchar](250) NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_User] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tb_UserRole]    Script Date: 2/11/2561 14:44:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tb_UserRole](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserID] [int] NOT NULL,
	[RoleID] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_tb_UserRole] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC,
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[tb_Menu] ON 
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (1, NULL, 1, N'Dashboards', N'Dashboard', N'Index', CAST(N'2018-11-02T12:02:13.390' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (2, 1, 1, N'Dashboard v.1', N'Dashboard', N'v1', CAST(N'2018-11-02T12:03:59.847' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (3, 1, 2, N'Dashboard v.2', N'Dashboard', N'v2', CAST(N'2018-11-02T12:04:17.940' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (4, 1, 3, N'Dashboard v.3', N'Dashboard', N'v3', CAST(N'2018-11-02T12:04:39.250' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (5, 1, 4, N'Dashboard v.4', N'Dashboard', N'v4', CAST(N'2018-11-02T12:05:13.867' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (6, 1, 5, N'Dashboard v.5', N'Dashboard', N'v5', CAST(N'2018-11-02T12:05:30.187' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (7, NULL, 2, N'Layouts', N'Layout', N'Index', CAST(N'2018-11-02T13:05:20.000' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (8, NULL, 3, N'Graphs', N'Graph', N'Index', CAST(N'2018-11-02T13:05:47.670' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (9, 8, 1, N'Flot Chart', N'Graph', N'FloatChart', CAST(N'2018-11-02T13:06:31.287' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (10, 8, 2, N'Morris.js', N'Graph', N'Morris', CAST(N'2018-11-02T13:06:57.543' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (11, 8, 3, N'Rickshaw Charts', N'Graph', N'Rickshow', CAST(N'2018-11-02T13:07:45.467' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (12, 8, 4, N'Chart.js', N'Graph', N'Chart', CAST(N'2018-11-02T13:08:09.163' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (13, 8, 5, N'Chartist', N'Graph', N'Chartist', CAST(N'2018-11-02T13:08:49.063' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (14, 8, 6, N'c3 charts', N'Graph', N'c3Chart', CAST(N'2018-11-02T13:09:19.133' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (15, 8, 7, N'Peity Charts', N'Graph', N'PeityChart', CAST(N'2018-11-02T13:09:52.960' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (16, 8, 8, N'Sparkline Charts', N'Graph', N'Sprarkline', CAST(N'2018-11-02T13:10:53.580' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (17, NULL, 4, N'Mailbox', N'Mail', N'Index', CAST(N'2018-11-02T13:11:15.197' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (18, NULL, 5, N'Metrics', N'Metric', N'Index', CAST(N'2018-11-02T13:11:41.817' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (19, NULL, 6, N'Widgets', N'Widget', N'Index', CAST(N'2018-11-02T13:12:04.403' AS DateTime), N'1', NULL, NULL)
GO
INSERT [dbo].[tb_Menu] ([MenuID], [MenuParentID], [MenuOrder], [MenuName], [Controller], [Action], [CreateDate], [CreateBy], [UpdateDate], [UpdateBy]) VALUES (20, NULL, 7, N'Forms', N'Form', N'Index', CAST(N'2018-11-02T13:12:26.743' AS DateTime), N'1', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[tb_Menu] OFF
GO
SET IDENTITY_INSERT [dbo].[tb_User] ON 
GO
INSERT [dbo].[tb_User] ([UserID], [UserName], [Password], [Email], [CreateDate]) VALUES (1, N'yutthana', N'password', N'email@email.com', CAST(N'2018-11-02T10:15:44.190' AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[tb_User] OFF
GO
ALTER TABLE [dbo].[tb_Menu] ADD  CONSTRAINT [DF_tb_Menu_MenuOrder]  DEFAULT ((1)) FOR [MenuOrder]
GO
ALTER TABLE [dbo].[tb_Menu] ADD  CONSTRAINT [DF_tb_Menu_CreateDatw]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[tb_Permission] ADD  CONSTRAINT [DF_tb_Permission_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[tb_Profile] ADD  CONSTRAINT [DF_tb_Profile_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[tb_Role] ADD  CONSTRAINT [DF_tb_Role_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[tb_RoleMenu] ADD  CONSTRAINT [DF_tb_RoleMenu_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[tb_User] ADD  CONSTRAINT [DF_tb_User_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO
ALTER TABLE [dbo].[tb_UserRole] ADD  CONSTRAINT [DF_tb_UserRole_CreateDate]  DEFAULT (getdate()) FOR [CreateDate]
GO

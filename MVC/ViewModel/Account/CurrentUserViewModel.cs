﻿using System.Collections.Generic;
using System.Data;

namespace MVC.ViewModel.Account
{
    public class CurrentUserViewModel
    {
        public string EmployeeCode { get; set; }

        public string DisplayName { get; set; }

        public string UserName { get; set; }

        public List<Rule> Rules { get; set; } = new List<Rule>();
    }
}

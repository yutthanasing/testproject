﻿using ApplicationModel.UtilityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.ViewModel.Navigator
{
    public class MenuViewModel
    {
        public List<MenuModel> MenuList { get; set; }
    }
}

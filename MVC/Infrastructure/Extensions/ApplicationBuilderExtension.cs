﻿using Microsoft.AspNetCore.Builder;
namespace MVC.Infrastructure.Extensions
{
    public static class ApplicationBuilderExtension
    {
        public static void UseProfiler(this IApplicationBuilder app, bool isEnableMiniProfiler)
        {
            if (isEnableMiniProfiler)
            {
                app.UseMiniProfiler();
            }
        }
    }
}

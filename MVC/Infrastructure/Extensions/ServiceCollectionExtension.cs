﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using MVC.Infrastructure.Interface;
using System;
using ApplicationCore.Entities;
using ApplicationCommon.Constants;
using Microsoft.AspNetCore.Authentication.Cookies;
using Application.MVC.Policies;
using Application.MVC.Interface;
using ApplicationUtility.Interface.Navigate;
using ApplicationUtility.Navigate;
using ApplicationCore.Interface;
using ApplicationCore.Repository;

namespace MVC.Infrastructure.Extensions
{
    public static class ServiceCollectionExtension
    {
        public static void RegisterDependencyInjection(this IServiceCollection services, IAppConfiguration appConfiguration)
        {
            //Common
            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IAppConfiguration, AppConfiguration>();
            services.AddScoped<IUserContext, UserContext>();
            services.AddScoped<IUserAuthentication, UserContext>();

            //Utility
            services.AddScoped<IMenuList , MenuList>();

            // Database Context
            services.AddSingleton(option => new ApplicationContextOptionBuilder(appConfiguration.GetApplicationConnection()));

            //Repository
            services.AddScoped<IMenuRepository, MenuRepository>();

        }

        public static void RegisterValidator(this IServiceCollection services)
        {
        }

        public static void RegisterAntiforgery(this IServiceCollection services)
        {
            services.AddAntiforgery(options =>
            {
                options.HeaderName = "X-XSRF-TOKEN";
                options.Cookie.Name = Startup.AntiforgeryName;
            });
        }

        public static void RegisterSession(this IServiceCollection services, int idleTimeoutFromMinutes)
        {
            services.AddSession(options =>
            {
                options.Cookie.Name = Startup.SessionName;
                options.IdleTimeout = TimeSpan.FromMinutes(idleTimeoutFromMinutes);
                options.Cookie.IsEssential = true;
            });
        }

        public static void RegisterCookiePolicy(this IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
        }

        public static void RegisterProfiler(this IServiceCollection services, bool isEnableMiniProfiler)
        {
            //if (isEnableMiniProfiler)
            //{
            //    services.AddMiniProfiler(options =>
            //    {
            //        options.RouteBasePath = "/profiler";
            //        ((MemoryCacheStorage)options.Storage).CacheDuration = TimeSpan.FromMinutes(60);
            //        options.SqlFormatter = new StackExchange.Profiling.SqlFormatters.InlineFormatter();
            //    });

            //    services.AddMiniProfiler()
            //        .AddEntityFramework();
            //}
        }

        public static void AddAuthorizationPolicies(this IServiceCollection services)
        {
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie(options =>
            {
                options.LoginPath = "/Account/Login/";
            });

            services.AddAuthorization(option =>
            {
                option.AddPolicy(Policies.Guest, policy => policy.Requirements.Add(new GuestRequirement()));
            });
        }

    }
}

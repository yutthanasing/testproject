﻿using Application.MVC.Attributes;
using Application.MVC.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using MVC.Infrastructure.Interface;
using System.Linq;
using System.Reflection;

namespace MVC.Infrastructure.Filters
{
    public class VerifyPermissionFilter : IActionFilter
    {
        public void OnActionExecuting(ActionExecutingContext context)
        {
            var userContext = context.HttpContext.Resolving<IUserContext>();
            var actionPermission = GetPermission(context);
            if (userContext != null && actionPermission != null && userContext.IsLogin())
            {
                bool canAccess = userContext.IsExistRule(actionPermission.Rules.ToArray());
                if (canAccess == false)
                {
                    context.Result = new RedirectToRouteResult(
                        new RouteValueDictionary
                        {
                            { "controller", "Home" },
                            { "action", "Index" }
                        });
                }
            }
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        private static PermissionAttribute GetPermission(ActionContext context) =>
            (context.ActionDescriptor as ControllerActionDescriptor)?.MethodInfo
            .GetCustomAttributes<PermissionAttribute>()
            .FirstOrDefault();
    }
}

﻿using ApplicationCommon;
using ApplicationCommon.Security.Interfaces;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using MVC.Infrastructure.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Infrastructure
{
    public class AppConfiguration : IAppConfiguration
    {
        private readonly IHostingEnvironment hostingEnvironment;
        private readonly IApplicationSecurity applicationSecurity;

        public AppConfiguration(IConfiguration configuration,
            IHostingEnvironment hostingEnvironment,
            IApplicationSecurity applicationSecurity)
        {
            Configuration = configuration;
            this.hostingEnvironment = hostingEnvironment;
            this.applicationSecurity = applicationSecurity;
        }

        public bool IsDevelopment => hostingEnvironment.IsDevelopment();

        public TimeSpan GetCachingDays()
        {
            var cachingDays = Configuration["CachingDays"];
            if (int.TryParse(cachingDays, out var value) == false)
            {
                throw new FormatException("Can't convert CachingDays to integer.");
            }
            return new TimeSpan(value, 0, 0, 0);
        }

        public string GetApplicationConnection()
        {
            var applicationConnection = Configuration["ConnectionStrings:ApplicationConnection"];
            ThrowIf.NullOrWhiteSpace(applicationConnection, nameof(applicationConnection));
            return applicationSecurity.Decrypt(applicationConnection);
        }

        public IConfiguration Configuration { get; }

        public int SessionTimeoutInMinutes => Configuration.GetValue<int>("SessionTimeoutInMinutes");
    }
}

﻿using System.Collections.Generic;
using ApplicationModel.UtilityModel;
using ApplicationUtility.Interface.Navigate;
using Microsoft.AspNetCore.Mvc;
using MVC.ViewModel.Navigator;

namespace MVC.Infrastructure.ViewComponents
{
    [ViewComponent(Name = "MenuView")]
    public class MenuViewComponent:ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            MenuViewModel model = new MenuViewModel();
            model.MenuList = menuList.BuildMenu();
            return View(model);
        }

        private readonly IMenuList menuList;

        public MenuViewComponent(IMenuList menuList)
        {
            this.menuList = menuList;
        }
    }
}

﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Infrastructure.Interface
{
    public interface IAppConfiguration
    {
        bool IsDevelopment { get; }

        TimeSpan GetCachingDays();

        string GetApplicationConnection();

        IConfiguration Configuration { get; }

        int SessionTimeoutInMinutes { get; }
    }
}

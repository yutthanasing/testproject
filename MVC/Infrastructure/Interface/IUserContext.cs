﻿using Application.MVC.Interface;
using MVC.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Infrastructure.Interface
{
    public interface IUserContext : IUserAuthentication
    {
        CurrentUserViewModel CurrentUser { get; set; }

        bool IsExistRule(params Rule[] rules);

        void Logout();
    }
}

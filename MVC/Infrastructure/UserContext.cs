﻿using Application.MVC.Extensions;
using Microsoft.AspNetCore.Http;
using MVC.Infrastructure.Interface;
using MVC.ViewModel.Account;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace MVC.Infrastructure
{
    public class UserContext : IUserContext
    {
        public CurrentUserViewModel CurrentUser
        {
            get => session?.GetObjectFromJson<CurrentUserViewModel>(nameof(CurrentUser));
            set => session?.SetObjectAsJson(nameof(CurrentUser), value);
        }

        public bool IsExistRule(params Rule[] rules)
        {
            var currentUser = CurrentUser;
            return currentUser != null && currentUser.Rules.Any(rules.Contains);
        }

        public void Logout()
        {
            session.Clear();
            response.Cookies.Delete(Startup.SessionName);
            response.Cookies.Delete(Startup.AntiforgeryName);
        }

        public bool IsLogin() => CurrentUser != null;

        #region Dependency Injection

        private readonly ISession session;

        private readonly HttpResponse response;

        public UserContext(IHttpContextAccessor httpContextAccessor)
        {
            session = httpContextAccessor?.HttpContext?.Session;
            response = httpContextAccessor?.HttpContext?.Response;
        }

        #endregion Dependency Injection
    }
}

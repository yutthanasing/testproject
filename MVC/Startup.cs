﻿using Application.MVC.Binders;
using Application.MVC.Extensions;
using ApplicationCommon.Security;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MVC.Infrastructure;
using MVC.Infrastructure.Extensions;
using MVC.Infrastructure.Filters;
using MVC.Infrastructure.Interface;
using Newtonsoft.Json.Serialization;
using NLog.Web;

namespace MVC
{
    public class Startup
    {

        public const string SessionName = "MVC.Session";

        public const string AntiforgeryName = "MVC.Security.Antiforgery";

        public IAppConfiguration AppConfiguration { get; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                 .SetBasePath(env.ContentRootPath)
                 .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                 .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                 .AddEnvironmentVariables();

            AppConfiguration = new AppConfiguration(builder.Build(), env, new DefaultApplicationSecurity());
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.RegisterProfiler(false);

            services.RegisterCookiePolicy();

            services.RegisterAntiforgery();

            services.RegisterSession(AppConfiguration.SessionTimeoutInMinutes);

            services.RegisterDependencyInjection(AppConfiguration);

            services.RegisterValidator();

            services
                .AddMvc(option =>
                {
                    option.ModelBinderProviders.Insert(0, new DefaultBinderProvider());
                    option.ModelBindingMessageProvider.CustomMessageProvider(AppConfiguration.IsDevelopment);

                    option.Filters.Add(new VerifyPermissionFilter());
                })
                .AddSessionStateTempDataProvider()
                //.AddFluentValidation(option =>
                //{
                //    option.RunDefaultMvcValidationAfterFluentValidationExecutes = false;
                //})
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddAuthorizationPolicies();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            env.ConfigureNLog($"nlog.{env.EnvironmentName}.config");

            app.UseProfiler(false);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error/Index");
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseFileCaching(AppConfiguration.GetCachingDays());

            app.UseCookiePolicy();

            app.UseSession();

            //app.UseRequestLocalization("AutoLoan.Backend.Language");

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }   

        
    }
}

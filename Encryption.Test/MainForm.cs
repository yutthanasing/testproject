﻿using ApplicationCommon.Security;
using ApplicationCommon.Security.Interfaces;
using System;
using System.Windows.Forms;

namespace Encryption.Test
{
	public partial class MainForm : Form
	{
		private readonly IApplicationSecurity applicationSecurity;

		public MainForm()
		{
			InitializeComponent();

			applicationSecurity = new DefaultApplicationSecurity();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			richTextBox1.Text = applicationSecurity.Encrypt(richTextBox1.Text);
		}

		private void button2_Click(object sender, EventArgs e)
		{
			richTextBox2.Text = applicationSecurity.Decrypt(richTextBox2.Text);
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
		}
	}
}
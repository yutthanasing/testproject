﻿using ApplicationCore.Interface;
using ApplicationModel.UtilityModel;
using ApplicationUtility.Interface.Navigate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace ApplicationUtility.Navigate
{
    public class MenuList: IMenuList
    {

        public readonly IMenuRepository menuRepository;
        public List<MenuModel> BuildMenu()
        {
            List<MenuModel> menus = new List<MenuModel>();
            menus = menuRepository.GetAllMenus();
            List<MenuModel> rtn = new List<MenuModel>();
            foreach (MenuModel menu in menus.Where(x => x.ParentID == null).OrderBy(o=>o.Order))
            {
                if(menus.Where(x=>x.ParentID ==menu .ID).Count() > 0)
                {
                    menu.IsParent = true;
                }
                rtn.Add(menu);
                rtn.AddRange(PopulateChildren(menu.ID, menus));
            }
            return rtn;
        }

        private List<MenuModel> PopulateChildren(int parentid,List<MenuModel> menus)
        {
            List<MenuModel> rtn = new List<MenuModel>();
            foreach(MenuModel item in menus.Where(x => x.ParentID == parentid).OrderBy(o => o.Order))
            {               
                if (menus.Where(x => x.ParentID == item.ID).ToList().Count > 0)
                {
                    item.IsParent = true;                    
                }
                rtn.Add(item);
                if (item.IsParent)
                {
                    rtn.AddRange(PopulateChildren(item.ID, menus));
                }
            }
            return rtn;
        }

        public MenuList(IMenuRepository menuRepository)
        {
            this.menuRepository = menuRepository;
        }
    }
}

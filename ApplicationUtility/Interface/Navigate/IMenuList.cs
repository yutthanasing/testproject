﻿using ApplicationModel.UtilityModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationUtility.Interface.Navigate
{
    public interface IMenuList
    {
        List<MenuModel> BuildMenu();
    }
}

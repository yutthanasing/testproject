﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationModel.UtilityModel
{
    public class MenuModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int? ParentID { get; set; }
        public int Order { get; set; }
        public string Controller { get; set; }
        public string ActionName { get; set; }
        public bool IsParent { get; set; }
        public IList<MenuModel> ChildMenu { get; set; }
    }
}

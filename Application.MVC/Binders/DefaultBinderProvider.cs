﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MVC.Binders
{
    public class DefaultBinderProvider : IModelBinderProvider
    {
        public IModelBinder GetBinder(ModelBinderProviderContext context)
        {
            if (context.Metadata.ModelType == typeof(DateTime))
            {
                return new DateTimeBinder(default(DateTime));
            }

            if (context.Metadata.ModelType == typeof(DateTime?))
            {
                return new DateTimeBinder(default(DateTime?));
            }

            return null;
        }
    }
}

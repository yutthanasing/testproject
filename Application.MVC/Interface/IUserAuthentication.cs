﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MVC.Interface
{
    public interface IUserAuthentication
    {
        bool IsLogin();
    }
}

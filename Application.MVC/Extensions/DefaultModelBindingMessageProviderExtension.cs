﻿using Microsoft.AspNetCore.Mvc.ModelBinding.Metadata;
using System;
using System.Collections.Generic;
using System.Text;

namespace Application.MVC.Extensions
{
    public static class DefaultModelBindingMessageProviderExtension
    {
        public static void CustomMessageProvider(this DefaultModelBindingMessageProvider provider, bool isDevelopment)
        {
            provider.SetAttemptedValueIsInvalidAccessor((value, propertyName) =>
                isDevelopment
                    ? $"The value '{value}' is not valid for {propertyName}."
                    : $"The value of '{propertyName}' is not valid.");
        }
    }
}

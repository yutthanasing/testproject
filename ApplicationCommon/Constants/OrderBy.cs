﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCommon.Constants
{
    public enum OrderBy
    {
        Ascending,
        Descending
    }
}

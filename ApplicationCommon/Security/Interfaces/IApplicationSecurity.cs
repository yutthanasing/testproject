﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCommon.Security.Interfaces
{
    public interface IApplicationSecurity
    {
        string Encrypt(string text);

        string Decrypt(string text);
    }
}

﻿using ApplicationCore.Entities;
using ApplicationCommon.Extensions;
using ApplicationModel.UtilityModel;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using ApplicationCore.Interface;

namespace ApplicationCore.Repository
{
    public class MenuRepository : IMenuRepository
    {
        private readonly ApplicationContextOptionBuilder builder;

        public MenuRepository(ApplicationContextOptionBuilder builder)
        {
            this.builder = builder;
        }

        public List<MenuModel> GetAllMenus()
        {
            using (var db = new ApplicationContext(builder))
            {
                return db.TbMenu
                           .Select(x => new MenuModel
                           {
                               ID = x.MenuId,
                               ParentID = x.MenuParentId,
                               ActionName = x.Action,
                               Controller = x.Controller,
                               Name = x.MenuName,
                               Order = x.MenuOrder
                           })
                           .ToList();
            }
        }
    }
}

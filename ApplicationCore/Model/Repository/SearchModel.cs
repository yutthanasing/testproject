﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Model.Repository
{
    public class SearchModel
    {
        public string Keyword { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
        public DateTime FormDate { get; set; }
        public DateTime ToDate { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }
        public string SortBy { get; set; }
        public bool SortDir { get; set; }
    }
}

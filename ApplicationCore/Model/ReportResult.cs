﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Model
{
    public class ReportResult<T> where T : class
    {
        public int MaximumElement { get; set; }

        public int MaximumPage { get; set; }

        public List<T> Entities { get; set; }

        public ReportResult()
        {
            Entities = new List<T>();
        }
    }
}

﻿using ApplicationModel.UtilityModel;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCore.Interface
{
    public interface  IMenuRepository
    {
        List<MenuModel> GetAllMenus();
    }
}

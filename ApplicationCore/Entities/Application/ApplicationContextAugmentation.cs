﻿using Microsoft.EntityFrameworkCore;
using System;

namespace ApplicationCore.Entities
{
    public partial class ApplicationContext
    {
        public ApplicationContext(ApplicationContextOptionBuilder option) : base(option.Build())
        {
        }

        [DbFunction]
        public static int ConvertToInt(string value)
        {
            throw new NotSupportedException("Can't connect to ConvertToInt scalar function.");
        }
    }
}

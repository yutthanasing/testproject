﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public partial class TbRole
    {
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public partial class TbUserRole
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public DateTime CreateDate { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public partial class TbPermission
    {
        public int PermissionId { get; set; }
        public string Permission { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}

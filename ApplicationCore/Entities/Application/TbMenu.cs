﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public partial class TbMenu
    {
        public int MenuId { get; set; }
        public int? MenuParentId { get; set; }
        public int MenuOrder { get; set; }
        public string MenuName { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public partial class TbRoleMenu
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int MenuId { get; set; }
        public int PermissionId { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
    }
}

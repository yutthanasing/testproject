﻿using System;
using System.Collections.Generic;

namespace ApplicationCore.Entities
{
    public partial class TbProfile
    {
        public int ProfileId { get; set; }
        public int UserId { get; set; }
        public string TitleName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Province { get; set; }
        public string District { get; set; }
        public string SubDistrict { get; set; }
        public string Mobile { get; set; }
        public string Phone { get; set; }
        public DateTime CreateDate { get; set; }
        public string CreateBy { get; set; }
        public DateTime? UpdateDate { get; set; }
        public string UpdateBy { get; set; }
    }
}

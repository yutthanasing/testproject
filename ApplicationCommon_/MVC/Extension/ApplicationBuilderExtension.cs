﻿using ApplicationCommon.Interface;
using ApplicationCommon.MVC.Middlewares;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.FileProviders;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace ApplicationCommon.MVC.Extension
{
    public static class ApplicationBuilderExtension
    {
        public static void UseFileCaching(this IApplicationBuilder app, TimeSpan cachingTime)
        {
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = responseContext =>
                {
                    var cacheHeader = "public,max-age=" + cachingTime.TotalSeconds.ToString("0");
                    responseContext.Context.Response.Headers[HeaderNames.CacheControl] = cacheHeader;
                }
            });
        }

        public static void UseFileServer(this IApplicationBuilder app, IFileServerConfiguration configuration)
        {
            var fileProvider = new PhysicalFileProvider(configuration.GetDirectory().FullName);

            app.UseFileServer(new FileServerOptions
            {
                FileProvider = fileProvider,
                RequestPath = configuration.GetRequestPrefix(),
                EnableDirectoryBrowsing = false
            });
        }

        public static void UseRequestLocalization(this IApplicationBuilder app, string cookieName)
        {
            var cultureEnglish = new CultureInfo("en-GB");

            var culture = new List<CultureInfo> { cultureEnglish };

            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                DefaultRequestCulture = new RequestCulture(cultureEnglish.Name, cultureEnglish.Name),
                SupportedCultures = new List<CultureInfo> { new CultureInfo("en-GB") },
                SupportedUICultures = culture,
                RequestCultureProviders = new List<IRequestCultureProvider>
                {
                    new CookieRequestCultureProvider
                    {
                        CookieName = cookieName
                    }
                }
            });
        }

        public static IApplicationBuilder UseAntiforgeryTokens(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ValidateAntiForgeryTokenMiddleware>();
        }
    }
}

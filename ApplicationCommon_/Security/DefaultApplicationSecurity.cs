﻿using ApplicationCommon.Security.Interface;
using System.Text;

namespace ApplicationCommon.Security
{
    public class DefaultApplicationSecurity : IApplicationSecurity
    {
        private const string Salt = "1aaf6ef0bf14d4208bb46a4cc";

        private const string Key = "fc577294c34e0b28ad283943594";

        private readonly Aes256 aes256;

        public DefaultApplicationSecurity()
        {
            aes256 = new Aes256(Encoding.UTF8.GetBytes(Salt));
        }

        public string Encrypt(string text)
        {
            return aes256.Encrypt(text, Key);
        }

        public string Decrypt(string text)
        {
            return aes256.Decrypt(text, Key);
        }
    }
}

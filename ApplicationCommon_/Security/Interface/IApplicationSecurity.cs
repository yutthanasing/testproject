﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCommon.Security.Interface
{
    public interface IApplicationSecurity
    {
        string Encrypt(string text);

        string Decrypt(string text);
    }
}

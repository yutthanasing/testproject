﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ApplicationCommon.Interface
{
    public interface IFileServerConfiguration
    {
        DirectoryInfo GetDirectory();

        string GetRequestPrefix();
    }
}

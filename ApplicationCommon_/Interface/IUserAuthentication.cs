﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApplicationCommon.Interface
{
    public interface IUserAuthentication
    {
        bool IsLogin();
    }
}
